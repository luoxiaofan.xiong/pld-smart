package fr.insalyon.hex4234.gateway;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

public class ServiceVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    vertx.eventBus().<String>consumer("service.courses.student").handler(message -> {
      getCoursesStudent(message.body()).onSuccess(array -> message.reply(new JsonObject().put("result", array)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
    });
    vertx.eventBus().<String>consumer("service.courses.professor").handler(message -> {
      getCoursesProf(message.body()).onSuccess(array -> message.reply(new JsonObject().put("result", array)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
    });
    vertx.eventBus().<Void>consumer("service.courses.admin").handler(message -> {
      getCoursesAdmin().onSuccess(array -> message.reply(new JsonObject().put("result", array)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
    });
    vertx.eventBus().<JsonObject>consumer("service.comments").handler(message -> {
      getComments(message.body().getString("username"), message.body().getString("fileid"),
          message.body().getInteger("page")).onSuccess(array -> message.reply(new JsonObject().put("result", array)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
    });
    startPromise.complete();
  }

  private Future<JsonArray> getFiles(String courseid) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject message = new JsonObject().put("courseid", courseid);
    vertx.eventBus().request("file.consult", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          promise.fail(file.encode());
        } else {
          promise.complete(file.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  // course list for students
  private Future<JsonArray> getCourses(Integer year) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject message = new JsonObject().put("year", year);

    vertx.eventBus().request("admin.consultCourse", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          promise.fail(course.encode());
        } else {
          promise.complete(course.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  // course list for prof
  private Future<JsonArray> getCourses(String profid) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject message = new JsonObject().put("professors",
        new JsonArray(Collections.unmodifiableList(new ArrayList<String>(Arrays.asList(profid)))));
    vertx.eventBus().request("admin.consultCourse", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          promise.fail(course.encode());
        } else {
          promise.complete(course.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  // course list for admin
  private Future<JsonArray> getCourses() {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject message = new JsonObject();
    vertx.eventBus().request("admin.consultCourse", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          promise.fail(course.encode());
        } else {
          promise.complete(course.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  private Future<Integer> getYear(String username) {
    Promise<Integer> promise = Promise.promise();
    JsonObject message = new JsonObject().put("username", username);
    vertx.eventBus().request("user.getInformation", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject user = (JsonObject) resp.result().body();

        if (user.containsKey("error")) {
          promise.fail(user.encode());
        } else {
          promise.complete(user.getInteger("year"));
        }
      }
    });
    return promise.future();
  }

  private Future<JsonArray> getCoursesStudent(String username) {
    return getYear(username).compose(year -> getCourses(year)).compose(cours -> {
      Promise<JsonArray> promise = Promise.promise();
      List<Future> list = new ArrayList<>();
      for (int i = 0; i < cours.size(); i++) {
        String courseid = cours.getJsonObject(i).getString("courseid");
        int j = i; // for fullfil lambda
        list.add(getFiles(courseid).onSuccess(array -> cours.getJsonObject(j).put("files", array)));
      }
      CompositeFuture.all(list).onSuccess(v -> promise.complete(cours)).onFailure(error -> promise.fail(error));

      return promise.future();
    });
  }

  private Future<JsonArray> getCoursesProf(String profid) {
    return getCourses(profid).compose(cours -> {
      Promise<JsonArray> promise = Promise.promise();
      List<Future> list = new ArrayList<>();
      for (int i = 0; i < cours.size(); i++) {
        String courseid = cours.getJsonObject(i).getString("courseid");
        int j = i; // for fullfil lambda
        list.add(getFiles(courseid).onSuccess(array -> cours.getJsonObject(j).put("files", array)));
      }
      CompositeFuture.all(list).onSuccess(v -> promise.complete(cours)).onFailure(error -> promise.fail(error));

      return promise.future();
    });
  }

  private Future<JsonArray> getCoursesAdmin() {
    return getCourses().compose(cours -> {
      Promise<JsonArray> promise = Promise.promise();
      List<Future> list = new ArrayList<>();
      for (int i = 0; i < cours.size(); i++) {
        String courseid = cours.getJsonObject(i).getString("courseid");
        int j = i; // for fullfil lambda
        list.add(getFiles(courseid).onSuccess(array -> cours.getJsonObject(j).put("files", array)));
      }
      CompositeFuture.all(list).onSuccess(v -> promise.complete(cours)).onFailure(error -> promise.fail(error));

      return promise.future();
    });
  }

  private Future<JsonArray> getComments(String fileid, Integer page) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject message = new JsonObject().put("fileid", fileid);
    if (page != null)
      message.put("page", page);
    vertx.eventBus().request("comment.consult", message, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject response = (JsonObject) resp.result().body();
        if (response.containsKey("error")) {
          promise.fail(response.encode());
        } else {
          promise.complete(response.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  private JsonArray filterComment(String username, JsonArray comments, List<String> commentid) {
    JsonArray filtered = new JsonArray();
    for (int i = 0; i < comments.size(); i++) {
      JsonObject object = comments.getJsonObject(i);
      if (!object.getBoolean("visibility")
          && !object.getString("username").equals(username)) {
        // refuse
      } else if (commentid.contains(object.getString("_id"))) {
        // add hidden tag
        object.put("hidden", true);
        filtered.add(object);
      } else {
        filtered.add(object);
      }
    }
    return filtered;
  }

  private Future<JsonArray> hiddenComment(String username) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject param = new JsonObject().put("username", username);
    vertx.eventBus().request("hiddenComment.consult", param, resp -> {
      if (resp.failed()) {
        promise.fail(resp.cause());
      } else {
        JsonObject response = (JsonObject) resp.result().body();
        if (response.containsKey("error")) {
          promise.fail(response.encode());
        } else {
          promise.complete(response.getJsonArray("result"));
        }
      }
    });
    return promise.future();
  }

  private List<String> hiddenCommentId(JsonArray hiddenComment) {
    List<String> list = new ArrayList<>();
    for (int i = 0; i < hiddenComment.size(); i++) {
      list.add(hiddenComment.getJsonObject(i).getString("commentid"));
    }
    return list;
  }

  private Future<JsonArray> getComments(String username, String fileid, Integer page) {
    return hiddenComment(username).map(this::hiddenCommentId).compose(list -> {
      return getComments(fileid, page).map(array -> this.filterComment(username, array, list));
    });
  }
}
