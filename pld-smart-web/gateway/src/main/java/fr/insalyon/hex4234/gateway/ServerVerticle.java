package fr.insalyon.hex4234.gateway;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.insalyon.hex4234.common.Util;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.OpenOptions;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.multipart.MultipartForm;

public class ServerVerticle extends AbstractVerticle {
  @Override
  public void start(Promise<Void> startPromise) throws Exception {
    HttpServer server = vertx.createHttpServer();

    Router router = Router.router(vertx);
    router.route("/css/*").handler(StaticHandler.create().setWebRoot("webroot/css"));

    // Login page do not need authentication
    router.route("/login.html").handler(StaticHandler.create().setIndexPage("login.html"));

    // Login api do not need authentication
    router.route("/api/user/login").handler(BodyHandler.create()).handler(this::handleLogin);

    // All api must be visited with authentication, return 401 directly if failed
    router.route("/api/*").handler(JWTAuthHandler.create(Util.getJWTAuth(vertx))).failureHandler(context -> {
      context.response().setStatusCode(401);
      context.response().end();
    });

    // All admin api must be admin
    router.route("/api/admin/*").handler(routingContext -> {
      if (routingContext.user().principal().getJsonArray("roles").getList().contains("admin")) {
        routingContext.next();
      } else {
        routingContext.response().setStatusCode(401);
        routingContext.response().end();
      }
    });

    // All user api must be user himself
    router.route("/api/user/:username").handler(routingContext -> {
      if (routingContext.user().principal().getString("username")
          .equals(routingContext.request().getParam("username"))) {
        routingContext.next();
      } else {
        routingContext.response().setStatusCode(401);
        routingContext.response().end();
      }
    });

    // All pages must be visited with authentication, redirect otherwise
    router.route().handler(StaticHandler.create());

    router.route("/static/*").handler(StaticHandler.create("file-uploads"));

    router.get("/api/admin/user").handler(this::handleAdminConsultUser);
    router.put("/api/admin/user").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleAdminAddUser);
    router.post("/api/admin/user").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleAdminUpdateUser);

    router.get("/api/admin/course").handler(this::handleAdminConsultCourse);
    router.put("/api/admin/course").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleAdminAddCourse);
    router.post("/api/admin/course").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleAdminUpdateCourse);
    router.delete("/api/admin/course").handler(this::handleAdminDeleteCourse);

    router.get("/api/user/:username").handler(this::handleUserGetInfo);
    router.post("/api/user/:username").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleUserUpdateInfo);

    router.get("/api/course/:courseid").handler(this::handleConsultCourse);
    router.get("/api/course").handler(this::handleConsultCourseList);

    router.get("/api/comment").handler(this::handleConsultComment);
    router.put("/api/comment").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleAddComment);
    router.post("/api/comment").handler(BodyHandler.create().setHandleFileUploads(false))
        .handler(this::handleModifyComment);
    router.delete("/api/comment").handler(this::handleDeleteComment);

    router.put("/api/comment/hide").handler(this::handleHideComment);
    router.delete("/api/comment/hide").handler(this::handleUnhideComment);

    router.get("/api/course/file").handler(this::handleConsultFile);
    router.post("/api/course/file").handler(BodyHandler.create(true)).handler(this::handleUploadFile);
    router.delete("/api/course/file").handler(this::handleDeleteFile);

    server.requestHandler(router).listen(8000);
  }

  private void handleLogin(RoutingContext context) {
    HttpServerRequest request = context.request();
    String username = request.getParam("username");
    String password = request.getParam("password");

    if (username == null || password == null) {
      context.response().setStatusCode(400);
      context.response().end(new JsonObject().put("error", "missing username or password").encode());
    } else {
      JsonObject message = new JsonObject().put("username", username).put("password", password);

      HttpServerResponse response = context.response();

      vertx.eventBus().request("user.login", message, result -> {
        if (result.failed()) {
          result.cause().printStackTrace();
          response.setStatusCode(500);
          response.end();
        } else {
          JsonObject token = (JsonObject) result.result().body();
          if (token.containsKey("token")) {
            response.setStatusCode(200);
            response.end(token.encode());
          } else {
            response.setStatusCode(401);
            response.end(token.encode());
          }
        }
      });
    }
  }

  private void handleUserGetInfo(RoutingContext context) {
    HttpServerRequest request = context.request();
    String username = request.getParam("username");

    if (username == null) {
      context.response().setStatusCode(400);
      context.response().end(new JsonObject().put("error", "missing username").encode());
    } else {
      JsonObject message = new JsonObject().put("username", username);
      HttpServerResponse response = context.response();

      vertx.eventBus().request("user.getInformation", message, result -> {
        if (result.failed()) {
          result.cause().printStackTrace();
          response.setStatusCode(500);
          response.end();
        } else {
          JsonObject token = (JsonObject) result.result().body();
          if (!token.containsKey("error")) {
            response.setStatusCode(200);
            response.end(token.encode());
          } else {
            response.setStatusCode(401);
            response.end(token.encode());
          }
        }
      });
    }
  }

  private void handleUserUpdateInfo(RoutingContext context) {
    HttpServerRequest request = context.request();
    String username = request.getParam("username");
    Optional<String> oldpassword = Optional.ofNullable(request.getParam("oldpassword"));
    Optional<String> newpassword = Optional.ofNullable(request.getParam("newpassword"));
    Optional<String> firstname = Optional.ofNullable(request.getParam("firstname"));
    Optional<String> lastname = Optional.ofNullable(request.getParam("lastname"));
    Optional<String> email = Optional.ofNullable(request.getParam("email"));

    if (username == null) {
      context.response().setStatusCode(400);
      context.response().end(new JsonObject().put("error", "missing username").encode());
    } else {
      HttpServerResponse response = context.response();

      JsonObject information = new JsonObject();
      information.put("username", username);
      oldpassword.ifPresent(s -> information.put("oldPassword", s));
      newpassword.ifPresent(s -> information.put("newPassword", s));
      firstname.ifPresent(s -> information.put("firstname", s));
      lastname.ifPresent(s -> information.put("lastname", s));
      email.ifPresent(s -> information.put("email", s));

      vertx.eventBus().request("user.updateInformation", information, result -> {
        if (result.failed()) {
          result.cause().printStackTrace();
          response.setStatusCode(500);
          response.end();
        } else {
          JsonObject object = (JsonObject) result.result().body();
          if (object.containsKey("error")) {
            context.response().setStatusCode(400);
            context.response().end(object.encode());
          } else {
            context.response().setStatusCode(200);
            context.response().end();
          }
        }
      });
    }
  }

  private void handleAdminAddUser(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();
    if (routingContext.request().getParam("username") == null
        || routingContext.request().getParam("password") == null) {
      response.setStatusCode(400);
      response.end("Bad request. Need username or password.");
      return;
    }

    JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username")).put("password",
        routingContext.request().getParam("password"));
    if (request.getParam("roles") != null)
      message.put("roles", new JsonArray(routingContext.request().params().getAll("roles")));
    if (request.getParam("year") != null) {
      message.put("year", Integer.parseInt(request.getParam("year")));
    }

    vertx.eventBus().request("admin.addUser", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        resp.cause().printStackTrace();
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end("user was added.");
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleAdminConsultUser(RoutingContext routingContext) {
    HttpServerRequest request = routingContext.request();
    HttpServerResponse response = routingContext.response();

    JsonObject message = new JsonObject();
    if (request.getParam("username") != null) {
      message.put("username", routingContext.request().getParam("username"));
    }
    if (!request.params().getAll("role").isEmpty()) {
      message.put("role", new JsonArray(routingContext.request().params().getAll("role")));
    }
    if (request.getParam("year") != null) {
      message.put("year", Integer.parseInt(request.getParam("year")));
    }

    vertx.eventBus().request("admin.consultUser", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        resp.cause().printStackTrace();
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleAdminUpdateUser(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    if (routingContext.request().getParam("username") == null) {
      response.setStatusCode(400);
      response.end("Bad request. Need username.");
      return;
    }
    JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username"));
    if (routingContext.request().getParam("roles") != null) {
      message.put("roles", new JsonArray(routingContext.request().params().getAll("roles")));
    }
    if (routingContext.request().getParam("year") != null)
      message.put("year", Integer.parseInt(routingContext.request().getParam("year")));

    if (routingContext.request().getParam("password") != null)
      message.put("password", routingContext.request().getParam("password"));

    vertx.eventBus().request("admin.updateUser", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end("user object was updated.");
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }

    });
  }

  private void handleAdminAddCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
        .put("coursename", routingContext.request().getParam("coursename"))
        .put("description", routingContext.request().getParam("description"))
        .put("professors", new JsonArray(routingContext.request().params().getAll("professors")))
        .put("year", Integer.parseInt(routingContext.request().getParam("year")));
    vertx.eventBus().request("admin.addCourse", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleConsultCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    MultiMap param = routingContext.request().params();
    JsonObject message = new JsonObject();
    message.put("courseid", param.get("courseid"));

    vertx.eventBus().request("course.consult", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          response.setStatusCode(400);
          response.end(course.encode());
        } else {
          response.end(course.encode());
        }
      }
    });
  }

  private void handleConsultCourseList(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();

    if (routingContext.user().principal().getJsonArray("roles").getList().contains("admin")) {
      vertx.eventBus().request("service.courses.admin", null, reply -> {
        if (reply.failed()) {
          response.setStatusCode(500);
          response.end(reply.cause().toString());
        } else {
          JsonObject resp = (JsonObject) reply.result().body();
          if (!resp.containsKey("error")) {
            response.setStatusCode(200);
            response.end(resp.encode());
          } else {
            response.setStatusCode(400);
            response.end(resp.encode());
          }
        }
      });
    } else if (routingContext.user().principal().getJsonArray("roles").getList().contains("student")) {
      vertx.eventBus().request("service.courses.student", routingContext.user().principal().getString("username"),
          reply -> {
            if (reply.failed()) {
              response.setStatusCode(500);
              response.end(reply.cause().toString());
            } else {
              JsonObject resp = (JsonObject) reply.result().body();
              if (!resp.containsKey("error")) {
                response.setStatusCode(200);
                response.end(resp.encode());
              } else {
                response.setStatusCode(400);
                response.end(resp.encode());
              }
            }
          });
    } else if (routingContext.user().principal().getJsonArray("roles").getList().contains("professor")) {
      vertx.eventBus().request("service.courses.professor", routingContext.user().principal().getString("username"),
          reply -> {
            if (reply.failed()) {
              response.setStatusCode(500);
              response.end(reply.cause().toString());
            } else {
              JsonObject resp = (JsonObject) reply.result().body();
              if (!resp.containsKey("error")) {
                response.setStatusCode(200);
                response.end(resp.encode());
              } else {
                response.setStatusCode(400);
                response.end(resp.encode());
              }
            }
          });
    }

    else {
      response.end();
    }
  }

  private void handleAdminConsultCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    MultiMap param = routingContext.request().params();
    JsonObject message = new JsonObject();
    if (param.contains("courseid"))
      message.put("courseid", param.get("courseid"));
    if (param.contains("year"))
      message.put("year", Integer.parseInt(param.get("year")));
    if (param.contains("professors"))
      message.put("professors", new JsonArray(param.getAll("professors")));

    vertx.eventBus().request("admin.consultCourse", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          response.setStatusCode(404);
          response.end(course.encode());
        } else {
          response.end(course.encode());
        }
      }
    });
  }

  private void handleAdminUpdateCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
        .put("coursename", routingContext.request().getParam("coursename"))
        .put("description", routingContext.request().getParam("description"))
        .put("professors", new JsonArray(routingContext.request().params().getAll("professors")))
        .put("year", routingContext.request().getParam("year"));
    vertx.eventBus().request("admin.updateCourse", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleAdminDeleteCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"));
    vertx.eventBus().request("admin.deleteCourse", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleAddComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("username", routingContext.user().principal().getString("username"))
        .put("courseid", routingContext.request().getParam("courseid"))
        .put("fileid", routingContext.request().getParam("fileid"))
        .put("content", routingContext.request().getParam("content"))
        .put("page", routingContext.request().getParam("page")).put("time", routingContext.request().getParam("time"))
        .put("visibility", routingContext.request().getParam("visibility"));
    vertx.eventBus().request("comment.add", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();
        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end(file.encode());
        } else {
          response.setStatusCode(200);
          response.end(file.encode());
        }
      }
    });
  }

  private void handleConsultComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("fileid", routingContext.request().getParam("fileid"));
    String page = routingContext.request().getParam("page");
    if (page != null)
      message.put("page", Integer.parseInt(page));
    String username = routingContext.user().principal().getString("username");
    message.put("username", username);

    vertx.eventBus().request("service.comments", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();
        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end(file.encode());
        } else {
          response.setStatusCode(200);
          response.end(file.encode());
        }
      }
    });

  }

  private void handleModifyComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("_id", routingContext.request().getParam("_id"))
        .put("username", routingContext.user().principal().getString("username"))
        .put("courseid", routingContext.request().getParam("courseid"))
        .put("fileid", routingContext.request().getParam("fileid"))
        .put("content", routingContext.request().getParam("content"))
        .put("page", routingContext.request().getParam("page")).put("time", routingContext.request().getParam("time"))
        .put("visibility", Boolean.parseBoolean(routingContext.request().getParam("visibility")));
    vertx.eventBus().request("comment.modify", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end(file.encode());
        } else {
          response.setStatusCode(200);
          response.end(file.encode());
        }

      }
    });
  }

  private void handleDeleteComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("_id", routingContext.request().getParam("_id"));
    vertx.eventBus().request("comment.delete", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());

          vertx.eventBus().send("hidenComment.delete",
              new JsonObject().put("commentid", routingContext.request().getParam("_id")));

        } else {
          response.setStatusCode(400);
        }
      }
    });
  }

  private void handleHideComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("username", routingContext.user().principal().getString("username"))
        .put("commentid", routingContext.request().getParam("commentid"))
        .put("fileid", routingContext.request().getParam("fileid"));
    vertx.eventBus().request("hiddenComment.add", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end(file.encode());
        } else {
          response.setStatusCode(200);
          response.end(file.encode());
        }

      }
    });
  }

  private void handleUnhideComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("commentid", routingContext.request().getParam("commentid"))
        .put("username", routingContext.user().principal().getString("username"));
    vertx.eventBus().request("hiddenComment.delete", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

  private void handleUploadFile(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();

    String courseid = routingContext.request().getParam("courseid");
    if (courseid == null) {
      response.setStatusCode(400);
      response.end(new JsonObject().put("error", "no courseid").encode());
      return;
    }
    vertx.fileSystem().mkdir("file-uploads/" + courseid, result -> {
    });
    for (FileUpload f : routingContext.fileUploads()) {
      // do whatever you need to do with the file (it is already saved
      // on the directory you wanted...
      vertx.fileSystem().move(f.uploadedFileName(), "file-uploads/" + courseid + "/" + f.fileName(), result -> {
        if (result.failed()) {
          response.setStatusCode(500);
          response.end(new JsonObject().put("error", result.cause().toString()).encode());
        } else {
          String size = String.valueOf(f.size() * 2);

          WebClient client = WebClient.create(vertx);
          // Upload file to python server
          MultipartForm formUpload = MultipartForm.create().binaryFileUpload("file", f.fileName(),
              "file-uploads/" + courseid + "/" + f.fileName(), "application/pdf");

          client.post(8080, "localhost", "/upload").putHeader("Content-Type", "multipart/form-data")
              .putHeader("Content-Length", size).putHeader("Host", "localhost")
              .putHeader("User-Agent", "PostmanRuntime/7.24.1").putHeader("Accept", "*/*")
              .putHeader("Accept-Encoding", "gzip, deflate, br").putHeader("Connection", "keep-alive")
              .sendMultipartForm(formUpload, ar -> {
                if (ar.succeeded()) {

                  HttpResponse<Buffer> rsp = ar.result();
                } else {
                  ar.cause().printStackTrace();
                }
              });
          MultipartForm formAnalyse = MultipartForm.create().attribute("pdfName", f.fileName()).attribute("dsNames",
              "null");

          // Submit the form as a multipart form body
          client.post(8080, "localhost", "/SummarizeDocument").putHeader("Content-Type", "multipart/form-data")
              .putHeader("Content-Length", "1000").putHeader("Host", "localhost")
              .putHeader("User-Agent", "PostmanRuntime/7.24.1").putHeader("Accept-Encoding", "gzip, deflate, br")
              .putHeader("Connection", "keep-alive").putHeader("Content-Type", "application/x-www-form-urlencoded")
              .sendMultipartForm(formAnalyse, ar -> {
                if (ar.succeeded()) {

                  HttpResponse<Buffer> rsp = ar.result();
                  JsonArray mJsonArray = rsp.bodyAsJsonArray();
                  StringBuilder buffer = new StringBuilder();
                  buffer.append("Synthèse du fichier : " + f.fileName() + "\n");
                  for (int i = 0; i < mJsonArray.size(); i++) {
                    JsonObject mJsonObject = mJsonArray.getJsonObject(i);
                    String title = mJsonObject.getString("titre");
                    buffer.append("\n\n"+title + "\n");

                    JsonArray eleMots = mJsonObject.getJsonArray("motsImportants");
                    Optional<String> keyWords = Stream.iterate(0, j -> j < eleMots.size(), j -> (j + 1))
                        .map(j -> eleMots.getString(j)).reduce((s1, s2) -> s1 + " " + s2);

                    keyWords.ifPresent(k -> buffer.append("Mots-Clés : " + k + "\n"));

                    JsonArray eleArr = mJsonObject.getJsonArray("ideesCles");
                    Optional<String> sentence = Stream.iterate(0, j -> j < eleArr.size(), j -> (j + 1))
                        .map(j -> eleArr.getString(j)).reduce((s1, s2) -> s1.trim() + " \n- " + s2.trim());
                    
                    
                    sentence.ifPresent(str -> buffer.append(str));
                  }
                  vertx.fileSystem().createFile("file-uploads/" + courseid + "/" + f.fileName().replace(".pdf", ".txt"),
                      Promise.<Void>promise().future());
                  vertx.fileSystem().open("file-uploads/" + courseid + "/" + f.fileName().replace(".pdf", ".txt"),
                      new OpenOptions().setAppend(true), file -> {
                        file.result().write(Buffer.buffer(buffer.toString()));
                      });

                } else {
                  ar.cause().printStackTrace();
                }
              });
        }
      });
      JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
          .put("filename", f.fileName());
      vertx.eventBus().request("file.upload", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(500);
          response.end(resp.cause().toString());
        } else {
          JsonObject file = (JsonObject) resp.result().body();

          if (file.containsKey("error")) {
            response.setStatusCode(400);
            response.end(file.encode());
          } else {
            response.setStatusCode(200);
            response.end(file.encode());
          }
        }
      });
    }

  }

  private void handleConsultFile(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("filename", routingContext.request().getParam("filename")).put("courseid",
        routingContext.request().getParam("courseid"));
    vertx.eventBus().request("file.consult", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(404);
          response.end("file not found");
        } else {
          response.setStatusCode(200);
          response.end(file.encode());
        }
      }
    });
  }

  private void handleDeleteFile(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    String courseid = routingContext.request().getParam("courseid");
    String filename = routingContext.request().getParam("filename");
    if (filename == null || courseid == null) {
      response.setStatusCode(400);
      response.end(new JsonObject().put("error", "filename and courseid needed").encode());
    }
    JsonObject message = new JsonObject().put("filename", filename).put("courseid", courseid);

    vertx.eventBus().request("file.delete", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(500);
        response.end(resp.cause().toString());
      } else {
        JsonObject result = (JsonObject) resp.result().body();
        if (!result.containsKey("error")) {
          vertx.fileSystem().delete("file-uploads/" + courseid + "/" + filename, r -> {
          });
          vertx.fileSystem().delete("file-uploads/" + courseid + "/" + filename.replace(".pdf", ".txt"), r -> {
          });

          response.setStatusCode(200);
          response.end(result.encode());
        } else {
          response.setStatusCode(400);
          response.end(result.encode());
        }
      }
    });
  }

}
