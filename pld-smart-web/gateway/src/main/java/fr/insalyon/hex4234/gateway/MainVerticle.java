package fr.insalyon.hex4234.gateway;

import fr.insalyon.hex4234.comment.CommentVerticle;
import fr.insalyon.hex4234.comment.HiddenCommentVerticle;
import fr.insalyon.hex4234.cours.CoursVerticle;
import fr.insalyon.hex4234.comment.CommentVerticle;
import fr.insalyon.hex4234.comment.HiddenCommentVerticle;
import fr.insalyon.hex4234.fichier.FichierVerticle;
import fr.insalyon.hex4234.utilisateur.CredentialVerticle;
import fr.insalyon.hex4234.utilisateur.UserInfoVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Promise<Void> startFuture) throws Exception {
    vertx.deployVerticle(new CredentialVerticle());
    vertx.deployVerticle(new UserInfoVerticle());
    vertx.deployVerticle(new CoursVerticle());
    vertx.deployVerticle(new FichierVerticle());
    vertx.deployVerticle(new ServiceVerticle());
    vertx.deployVerticle(new ServerVerticle());
    vertx.deployVerticle(new CommentVerticle());
    vertx.deployVerticle(new HiddenCommentVerticle());
    startFuture.complete();
  }
}
