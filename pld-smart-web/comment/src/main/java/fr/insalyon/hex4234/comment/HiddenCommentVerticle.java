package fr.insalyon.hex4234.comment;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

public class HiddenCommentVerticle extends AbstractVerticle {

  private MongoClient client = null;
  private static final String COLLECTION = "hidenComment";

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    JsonObject mongoConfig = new JsonObject().put("db_name", "comment").put("host", "127.0.0.1").put("port", 27017);
    client = MongoClient.create(vertx, mongoConfig);

    // create comment and set index if not done
    findHidenCommentTable().<Void>compose(exists -> {
      if (exists) {
        // Do nothing;
        Promise<Void> promise = Promise.promise();
        promise.complete();
        return promise.future();
      } else {
        // create a collection "comment" and then add index
        return createHiddenCommentCollection().<Void>compose(v -> createHiddenCommentIndex());
      }
    });

    EventBus eventbus = vertx.eventBus();

    // event bus for request around creating a Comment
    eventbus.<JsonObject>consumer("hiddenComment.add").handler(message -> {
      JsonObject object = message.body();
      createHiddenComment(object.getString("username"), object.getString("commentid"), object.getString("fileid"))
          .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for request around consulting a Comment
    eventbus.<JsonObject>consumer("hiddenComment.consult").handler(message -> {
      JsonObject object = message.body();
      consultHiddenComment(object.getString("username"), object.getString("fileid"))
          .onSuccess(array -> message.reply(new JsonObject().put("result", array)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for request around removing a Comment
    eventbus.<JsonObject>consumer("hiddenComment.delete").handler(message -> {
      JsonObject object = message.body();
      deleteHiddenComment(object.getString("username"), object.getString("commentid"))
        .onSuccess(v -> message.reply(new JsonObject().put("success", v)))
        .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });
  }

  /**
   * Find if the user comment table exists
   */
  private Future<Boolean> findHidenCommentTable() {
    Promise<Boolean> promise = Promise.promise();
    client.getCollections(event -> {
      // if it fails, we pass along the cause
      if (event.failed()) {
        promise.fail(event.cause());
      }
      // otherwise, we pass the result
      else {
        promise.complete(event.result().contains(COLLECTION));
      }
    });
    // give back the future object representing this promise
    return promise.future();
  }

  /**
   * Create the user comment table if not exists
   */
  private Future<Void> createHiddenCommentCollection() {
    Promise<Void> promise = Promise.promise();
    client.createCollection(COLLECTION, event -> {
      if (event.failed()) {
        promise.fail(event.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Create the comment index for comment
   */
  private Future<Void> createHiddenCommentIndex() {
    Promise<Void> promise = Promise.promise();
    client.createIndexWithOptions(COLLECTION, new JsonObject().put("commentid", 1).put("username", 1),
        new IndexOptions().unique(true), event -> {
          if (event.failed()) {
            promise.fail(event.cause());
          } else {
            promise.complete();
          }
        });
    return promise.future();
  }

  private Future<Void> createHiddenComment(String username, String commentid, String fileid) {
    Promise<Void> promise = Promise.promise();
    JsonObject comment = new JsonObject().put("username", username).put("commentid", commentid).put("fileid", fileid);
    client.insert(COLLECTION, comment, res2 -> {
      if (res2.succeeded()) {
        promise.complete();
      } else {
        promise.fail(res2.cause());
      }
    });
    return promise.future();
  }

  private Future<JsonArray> consultHiddenComment(String username, String fileid) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject commentInfo = new JsonObject().put("username", username);
    if (fileid != null) new JsonObject().put("fileid", fileid);

    client.find(COLLECTION, commentInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(new JsonArray(res.result()));
      }
    });
    return promise.future();
  }

  private Future<Boolean> deleteHiddenComment(String username, String commentid) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject commentInfo = new JsonObject();
    if (username != null) commentInfo.put("username", username);
    if (commentid != null) commentInfo.put("commentid", commentid);
    client.findOneAndDelete(COLLECTION, commentInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result() != null);
      }
    });
    return promise.future();

  }

}
