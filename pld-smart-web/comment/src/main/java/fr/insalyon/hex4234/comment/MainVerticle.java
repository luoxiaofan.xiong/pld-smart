package fr.insalyon.hex4234.comment;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    HttpServer server = vertx.createHttpServer();

    Router router = Router.router(vertx);

    router.get("/comment").handler(this::handleConsultComment);
    // this will get the comment information into RoutingContext
    router.put("/comment").handler(this::handleAddComment);
    // this will put the comment information into RoutingContext
    router.post("/comment").handler(this::handleModifyComment);
    // this will update the comment information into RoutingContext
    router.delete("/comment").handler(this::handleDeleteComment);
    // this will put the comment information into RoutingContext

    router.get("/hidencomment").handler(this::handleConsultHidenComment);
    // this will get the hiden comment information into RoutingContext
    router.put("/hidencomment").handler(this::handleAddHidenComment);
    // this will put the hiden comment information into RoutingContext
    //router.post("/hidencomment").handler(this::handleModifyHidenComment);
    // this will update the hiden comment information into RoutingContext
    router.delete("/hidencomment").handler(this::handleDeleteHidenComment);
    // this will put the hiden comment information into RoutingContext

    server.requestHandler(router).listen(8080);

    vertx.deployVerticle(new CommentVerticle());
    vertx.deployVerticle(new HiddenCommentVerticle());
  }

  private void handleAddComment(RoutingContext routingContext) {
    // check if it's student
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("role").contains("student")) {
      JsonObject message = new JsonObject()
        .put("username", routingContext.request().getParam("username"))
        .put("courseid", routingContext.request().getParam("courseid"))
        .put("fileid", routingContext.request().getParam("fileid"))
        .put("content", routingContext.request().getParam("content"))
        .put("page", routingContext.request().getParam("page"))
        .put("time", routingContext.request().getParam("time"))
        .put("visibility", routingContext.request().getParam("visibility"));
      vertx.eventBus().request("comment.add", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          JsonObject file = (JsonObject) resp.result().body();

          if (file.containsKey("error")) {
            response.setStatusCode(400);
            response.end("Comment is not added");
          } else {
            response.setStatusCode(200);
            response.end("Comment is added");
            //Comment is added in the comment table
          }

        }
      });
    //} else {
    //  response.setStatusCode(401);
    //}
  }




  private void handleConsultComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("_id", routingContext.request().getParam("_id"));
    vertx.eventBus().request("comment.consult", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end("Can not consult comment");
        } else {
          response.setStatusCode(200);
          response.end("Comment consulted");
        }

      }
    });
  }
  private void handleModifyComment(RoutingContext routingContext) {
    // check if it's admin
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("roles").contains("student")) {
      //Chaque user peux que modifier les commentaire écirts par luimême
      //if (routingContext.user().principal().getJsonArray("username").contains(routingContext.request().getParam("owner"))) {
        JsonObject message = new JsonObject()
          .put("_id", routingContext.request().getParam("_id"))
          .put("username", routingContext.request().getParam("username"))
          .put("courseid", routingContext.request().getParam("courseid"))
          .put("fileid", routingContext.request().getParam("fileid"))
          .put("content", routingContext.request().getParam("content"))
          .put("page", routingContext.request().getParam("page"))
          .put("time", routingContext.request().getParam("time"))
          .put("visibility", routingContext.request().getParam("visibility"));
        vertx.eventBus().request("comment.modify", message, resp -> {
          if (resp.failed()) {
            response.setStatusCode(400);
            response.end(resp.cause().toString());
          } else {
            JsonObject file = (JsonObject) resp.result().body();

            if (file.containsKey("error")) {
              response.setStatusCode(400);
              response.end("Error in modification");
            } else {
              response.setStatusCode(200);
              response.end("Comment modiffied");
            }

          }
        });
      //} else {
      //  response.setStatusCode(401);
      //}
    //}
  }
    private void handleDeleteComment(RoutingContext routingContext) {
      // check if it's admin
      HttpServerResponse response = routingContext.response();
      //if (routingContext.user().principal().getJsonArray("role").contains("student")) {

        //Chaque user peux que supprimer les commentaire écirts par luimême
        //if(routingContext.user().principal().getJsonArray("username").contains(routingContext.request().getParam("owner"))) {
          JsonObject message = new JsonObject().put("_id", routingContext.request().getParam("_id"));
          vertx.eventBus().request("comment.delete", message, resp -> {
            if (resp.failed()) {
              response.setStatusCode(400);
              response.end(resp.cause().toString());
            } else {
              Boolean result = (Boolean) resp.result().body();
              if (result) {
                response.setStatusCode(200);
                response.end("deleted successfully");

                vertx.eventBus().request("hidenComment.delete", message, resp2 -> {
                  if (resp2.failed()) {
                    response.setStatusCode(400);
                    response.end(resp2.cause().toString());
                  } else {
                    Boolean result2 = (Boolean) resp2.result().body();
                    if (result) {
                      response.setStatusCode(200);
                      response.end("user comment deleted successfully");

                    }
                  }
                });

              }
            }
          });
        }
      //}
      /*else{
        response.setStatusCode(401);
      }
    }*/

  private void handleAddHidenComment(RoutingContext routingContext) {
    // check if it's student
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("role").contains("student")) {
    JsonObject message = new JsonObject()
      .put("username", routingContext.request().getParam("username"))
      .put("commentid", routingContext.request().getParam("commentid"))//??????
      .put("fileid", routingContext.request().getParam("fileid"));
    vertx.eventBus().request("hidenComment.add", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end("user Comment is not added");
        } else {
          response.setStatusCode(200);
          response.end("user Comment is added");
          //Comment is added in the comment table
        }

      }
    });
    //} else {
    //  response.setStatusCode(401);
    //}
  }
  private void handleDeleteHidenComment(RoutingContext routingContext) {
    // check if it's admin
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("role").contains("student")) {

    //Chaque user peux que supprimer les commentaire écirts par luimême
    //if(routingContext.user().principal().getJsonArray("username").contains(routingContext.request().getParam("owner"))) {
    JsonObject message = new JsonObject().put("commentid", routingContext.request().getParam("commentid"));
    vertx.eventBus().request("comment.delete", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        Boolean result = (Boolean) resp.result().body();
        if (result) {
          response.setStatusCode(200);
          response.end("deleted successfully");

          vertx.eventBus().request("hidenComment.delete", message, resp2 -> {
            if (resp2.failed()) {
              response.setStatusCode(400);
              response.end(resp2.cause().toString());
            } else {
              Boolean result2 = (Boolean) resp2.result().body();
              if (result) {
                response.setStatusCode(200);
                response.end("user comment deleted successfully");

              }
            }
          });

        }
      }
    });
  }
  //}
      /*else{
        response.setStatusCode(401);
      }
    }*/

  private void handleConsultHidenComment(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("_id", routingContext.request().getParam("_id"));
    vertx.eventBus().request("hidenComment.consult", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        JsonObject file = (JsonObject) resp.result().body();

        if (file.containsKey("error")) {
          response.setStatusCode(400);
          response.end("Can not consult comment");
        } else {
          response.setStatusCode(200);
          response.end("Comment consulted");
        }

      }
    });
  }

}
