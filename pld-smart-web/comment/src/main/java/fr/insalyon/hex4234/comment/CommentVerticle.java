package fr.insalyon.hex4234.comment;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.mongo.MongoClient;

public class CommentVerticle extends AbstractVerticle {

  private MongoClient client = null;

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    JsonObject mongoConfig = new JsonObject().put("db_name", "comment").put("host", "127.0.0.1").put("port", 27017);
    client = MongoClient.create(vertx, mongoConfig);

    // create comment and set index if not done
    findGeneralCommentTable().<Void>compose(exists -> {
      if (exists) {
        // Do nothing;
        Promise<Void> promise = Promise.promise();
        promise.complete();
        return promise.future();
      } else {
        // create a collection "comment"
        return createGeneralCollection();
      }
    });

    EventBus eventbus = vertx.eventBus();

    // event bus for request around creating a Comment
    eventbus.<JsonObject>consumer("comment.add").handler(message -> {
      JsonObject object = message.body();
      createComment(object.getString("username"), object.getString("courseid"), object.getString("fileid"),
          object.getString("content"), Integer.parseInt(object.getString("page")), object.getString("time"),
          Boolean.parseBoolean(object.getString("visibility")))
              .onSuccess(v -> message.reply(new JsonObject().put("_id", v)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error)));

    });

    // event bus for request around consulting a Comment
    eventbus.<JsonObject>consumer("comment.consultOne").handler(message -> {
      JsonObject object = message.body();
      consultComment(object.getString("_id")).onSuccess(v -> message.reply(new JsonObject().put("result", v)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));

    });

    // event bus for request around consulting a Comment
    eventbus.<JsonObject>consumer("comment.consult").handler(message -> {
      JsonObject object = message.body();
      consultComments(object.getString("fileid"), object.getInteger("page"))
          .onSuccess(v -> message.reply(new JsonObject().put("result", v)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));

    });

    // event bus for request around modifing a Comment
    eventbus.<JsonObject>consumer("comment.modify").handler(message -> {
      JsonObject object = message.body();
      modifyComment(object.getString("_id"), object.getString("content"), object.getString("time"),
          object.getBoolean("visibility")).onSuccess(v -> message.reply(new JsonObject().put("success", v)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));

    });

    // event bus for request around removing a Comment
    eventbus.<JsonObject>consumer("comment.delete").handler(message -> {
      JsonObject object = message.body();
      deleteComment(object.getString("_id")).onSuccess(b -> message.reply(new JsonObject().put("success", b)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
    });

  }

  /**
   * Find if the general comment table exists
   */
  private Future<Boolean> findGeneralCommentTable() {
    Promise<Boolean> promise = Promise.promise();
    client.getCollections(event -> {
      // if it fails, we pass along the cause
      if (event.failed()) {
        promise.fail(event.cause());
      }
      // otherwise, we pass the result
      else {
        promise.complete(event.result().contains("comment"));
      }
    });
    // give back the future object representing this promise
    return promise.future();
  }

  /**
   * Create the general comment table if not exists
   */
  private Future<Void> createGeneralCollection() {
    Promise<Void> promise = Promise.promise();
    client.createCollection("comment", event -> {
      if (event.failed()) {
        promise.fail(event.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Create comment
   */
  private Future<String> createComment(String username, String courseid, String fileid, String content, int page,
      String time, boolean visibility) {
    Promise<String> promise = Promise.promise();
    JsonObject comment = new JsonObject().put("username", username).put("courseid", courseid).put("fileid", fileid)
        .put("content", content).put("page", page).put("time", time).put("visibility", visibility);
    client.insert("comment", comment, result -> {
      if (result.succeeded()) {
        promise.complete(result.result());
      } else {
        promise.fail(result.cause());
      }
    });
    return promise.future();
  }

  private Future<Boolean> modifyComment(String _id, String content, String time, Boolean visibility) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject comment = new JsonObject().put("_id", _id);
    JsonObject newComment = new JsonObject().put("_id", _id);
    if (content != null)
      newComment.put("content", content);
    if (visibility != null)
      newComment.put("visibility", visibility);
    if (time != null)
      newComment.put("time", time);
    client.findOneAndUpdate("comment", comment, new JsonObject().put("$set", newComment), res2 -> {
      if (res2.succeeded()) {
        promise.complete(res2.result() != null);
      } else {
        promise.fail(res2.cause());
      }
    });
    return promise.future();
  }

  private Future<JsonObject> consultComment(String _id) {
    Promise<JsonObject> promise = Promise.promise();
    JsonObject commentInfo = new JsonObject().put("_id", _id);
    JsonObject fields = new JsonObject().put("username", true).put("fileid", true).put("page", true)
        .put("content", true).put("time", true).put("visibility", true);

    client.findOne("comment", commentInfo, fields, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result());
      }
    });
    return promise.future();
  }

  private Future<JsonArray> consultComments(String fileid, Integer page) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject commentInfo = new JsonObject();
    if (fileid != null)
      commentInfo.put("fileid", fileid);
    if (page != null)
      commentInfo.put("page", page);

    client.find("comment", commentInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(new JsonArray(res.result()));
      }
    });
    return promise.future();
  }

  private Future<Boolean> deleteComment(String _id) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject commentInfo = new JsonObject().put("_id", _id);
    client.findOneAndDelete("comment", commentInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result() != null);
      }
    });
    return promise.future();

  }

}
