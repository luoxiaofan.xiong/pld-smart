package fr.insalyon.hex4234.fichier;

import io.vertx.ext.mongo.IndexOptions;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;

public class FichierVerticle extends AbstractVerticle {

    private MongoClient client = null;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        JsonObject mongoConfig = new JsonObject().put("db_name", "file").put("host", "127.0.0.1").put("port", 27017);
        client = MongoClient.create(vertx, mongoConfig);

        // create file and set index if not done
        findFileTable().<Void>compose(exists -> {
            if (exists) {
                // Do nothing;
                Promise<Void> promise = Promise.promise();
                promise.complete();
                return promise.future();
            } else {
                // create a collection "file" and then add index
                return createCollection().<Void>compose(v -> createFileIndex());
            }
        });
        EventBus eventbus = vertx.eventBus();

        // event bus for request around consult a file
        eventbus.<JsonObject>consumer("file.consult").handler(message -> {
            JsonObject object = message.body();
            searchFile(object.getString("filename"), object.getString("courseid"))
                    .onSuccess(array -> message.reply(new JsonObject().put("result", array)))
                    .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
        });

        // event bus for request around uploading a file
        eventbus.<JsonObject>consumer("file.upload").handler(message -> {
            JsonObject object = message.body();
            uploadFile(object.getString("courseid"), object.getString("filename"))
                    .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                    .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
        });

        // event bus for request around deleting a file
        eventbus.<JsonObject>consumer("file.delete").handler(message -> {
            JsonObject object = message.body();

            deleteFile(object.getString("filename"), object.getString("courseid"))
                    .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                    .onFailure(error -> message.reply(new JsonObject().put("error", error.toString())));
        });
    }

    private Future<Boolean> findFileTable() {
        Promise<Boolean> promise = Promise.promise();
        client.getCollections(event -> {
            // if it fails, we pass along the cause
            if (event.failed()) {
                promise.fail(event.cause());
            } // otherwise, we pass the result
            else {
                promise.complete(event.result().contains("file"));
            }
        });
        // give back the future object representing this promise
        return promise.future();
    }

    /**
     * Create the file table if not exists
     */
    private Future<Void> createCollection() {
        Promise<Void> promise = Promise.promise();
        client.createCollection("file", event -> {
            if (event.failed()) {
                promise.fail(event.cause());
            } else {
                promise.complete();
            }
        });
        return promise.future();
    }

    /**
     * Create the file index
     *
     */
    private Future<Void> createFileIndex() {
        Promise<Void> promise = Promise.promise();
        client.createIndexWithOptions("file", new JsonObject().put("filename", 1).put("courseid", 1),
                new IndexOptions().unique(true), event -> {
                    if (event.failed()) {
                        promise.fail(event.cause());
                    } else {
                        promise.complete();
                    }
                });
        return promise.future();
    }

    /**
     * Add File
     */
    private Future<Void> uploadFile(String courseid, String filename) {
        Promise<Void> promise = Promise.promise();
        JsonObject file = new JsonObject().put("filename", filename).put("courseid", courseid);

        client.insert("file", file, result -> {
            if (result.succeeded()) {
                promise.complete();
            } else {
                promise.fail(result.cause());
            }
        });

        return promise.future();
    }

    private Future<JsonArray> searchFile(String filename, String courseid) {
        Promise<JsonArray> promise = Promise.promise();
        JsonObject fileInfo = new JsonObject();
        if (filename != null)
            fileInfo.put("filename", filename);
        if (courseid != null)
            fileInfo.put("courseid", courseid);
        client.find("file", fileInfo, res -> {
            if (res.failed()) {
                promise.fail(res.cause());
            } else {
                promise.complete(new JsonArray(res.result()));
            }
        });
        return promise.future();
    }

    /**
     * Delete File
     */
    private Future<Boolean> deleteFile(String filename, String courseid) {
        Promise<Boolean> promise = Promise.promise();
        JsonObject fileInfo = new JsonObject().put("filename", filename).put("courseid", courseid);
        client.findOneAndDelete("file", fileInfo, res -> {
            if (res.failed()) {
                promise.fail(res.cause());
            } else {
                promise.complete(res.result() != null);
            }
        });
        return promise.future();

    }

    /**
     * Modify File
     */
}
