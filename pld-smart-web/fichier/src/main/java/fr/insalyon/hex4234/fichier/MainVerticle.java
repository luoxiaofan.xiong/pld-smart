package fr.insalyon.hex4234.fichier;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.multipart.MultipartForm;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainVerticle extends AbstractVerticle {

    //private static Logger log = Logger.getLogger(MainVerticle.class.getName());
    private static Logger log = Logger.getLogger("org.mongodb.driver");
    private WebClient client = null;
    private TxtGenerator txtGenerator = null;

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        txtGenerator = new TxtGenerator();
        Router router = Router.router(vertx);
        HttpServer server = vertx.createHttpServer();
        WebClientOptions options = new WebClientOptions()
                .setUserAgent("My-App/1.2.3");
        options.setKeepAlive(false);
        client = WebClient.create(vertx, options);
        router.get("/course/file").handler(this::handleConsultFile);

        router.post("/course/file").handler(BodyHandler.create(true).setUploadsDirectory("./"));

        router.post("/course/file").handler(this::handleUploadFile);

        router.delete("/course/file").handler(this::handleDeleteFile);

        server.requestHandler(router).listen(8889);

        vertx.deployVerticle(new FichierVerticle());

    }

    private void handleUploadFile(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        log.info("Upload");
        String lectureName = "";
        String dsName = "";
        for (FileUpload f : routingContext.fileUploads()) {
            // do whatever you need to do with the file (it is already saved
            // on the directory you wanted...
            File uploadedFile = new File(f.uploadedFileName());
            String fileName = f.fileName();

            uploadedFile.renameTo(new File(fileName));

            String size = String.valueOf(f.size() * 2);

            log.info("Size: " + f.size());
            //Verify if the file uploaded is a ds
            String isDS = "";
            if (fileName.contains("ds")) {
                isDS = "true";
                dsName = fileName;
            } else {
                isDS = "false";
                lectureName = fileName;
            }
            String fileNameCourt = fileName.substring(0, fileName.length() - 4);
            try {
                log.info("Filename: " + fileNameCourt);
                TxtGenerator.creatTxtFile(fileNameCourt, "./");
            } catch (IOException ex) {
                Logger.getLogger(MainVerticle.class.getName()).log(Level.SEVERE, null, ex);
            }

            //Upload file to python server
            MultipartForm formUpload = MultipartForm.create()
                    .attribute("StoreDS", isDS)
                    .binaryFileUpload("file", fileName, "./" + fileName, "application/pdf");

            client
                    .post(8080, "localhost", "/upload")
                    .putHeader("Content-Type", "multipart/form-data")
                    .putHeader("Content-Length", size)
                    .putHeader("Host", "localhost")
                    .putHeader("User-Agent", "PostmanRuntime/7.24.1")
                    .putHeader("Accept", "*/*")
                    .putHeader("Accept-Encoding", "gzip, deflate, br")
                    .putHeader("Connection", "keep-alive")
                    .sendMultipartForm(formUpload, ar -> {
                        // .sendForm(form, ar -> {
                        log.info("Send file");
                        if (ar.succeeded()) {

                            HttpResponse<Buffer> rsp = ar.result();

                            log.info("Received response with status code" + String.valueOf(rsp.statusCode()) + rsp.bodyAsString());
                        } else {
                            log.info("Something went wrong " + ar.cause().getMessage());
                        }
                    });
            MultipartForm formAnalyse = MultipartForm.create()
                    .attribute("pdfName", lectureName)
                    .attribute("dsNames", "null");

            // Submit the form as a multipart form body
            client
                    .post(8080, "localhost", "/SummarizeDocument")
                    .putHeader("Content-Type", "multipart/form-data")
                    .putHeader("Content-Length", "1000")
                    .putHeader("Host", "localhost")
                    .putHeader("User-Agent", "PostmanRuntime/7.24.1")
                    .putHeader("Accept-Encoding", "gzip, deflate, br")
                    .putHeader("Connection", "keep-alive")
                    .putHeader("Content-Type", "application/x-www-form-urlencoded")
                    .sendMultipartForm(formAnalyse, ar -> {
                        // .sendForm(form, ar -> {
                        log.info("Send file");
                        if (ar.succeeded()) {

                            HttpResponse<Buffer> rsp = ar.result();
                            JsonArray mJsonArray = rsp.bodyAsJsonArray();
                            //JsonObject newJSON = jsonObject.getJsonObject("stat");
                            log.info("Received response with status code" + String.valueOf(rsp.statusCode()) + rsp.bodyAsString());
                            log.info("Len; " + String.valueOf(mJsonArray.size()));
                            JsonObject mJsonObject = new JsonObject();
                            for (int i = 0; i < mJsonArray.size(); i++) {
                                mJsonObject = mJsonArray.getJsonObject(i);
                                String title = mJsonObject.getString("titre");
                                log.info(title);

                                try {
                                    TxtGenerator.writeTxtFile(title, "./", fileNameCourt);
                                } catch (IOException ex) {
                                    Logger.getLogger(MainVerticle.class.getName()).log(Level.SEVERE, null, ex);
                                }

                                JsonArray eleMots = null;
                                eleMots = mJsonObject.getJsonArray("motsImportants");
                                //JsonObject eleObj = new JsonObject();
                                String keyWords = "Mots-Clés : ";
                                for (int j = 0; j < eleMots.size(); j++) {
                                    String keyWord = eleMots.getString(j);
                                    keyWords = keyWords + keyWord + " ";

                                    log.info(eleMots.getString(j));
                                }
                                if (!keyWords.trim().contentEquals("Mots-Clés :")) {
                                    try {
                                        TxtGenerator.writeTxtFile(keyWords, "./", fileNameCourt);
                                    } catch (IOException ex) {
                                        Logger.getLogger(MainVerticle.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                }

                                JsonArray eleArr = null;
                                eleArr = mJsonObject.getJsonArray("ideesCles");

                                for (int j = 0; j < eleArr.size(); j++) {
                                    try {
                                        String sentence = " - " + eleArr.getString(j);
                                        TxtGenerator.writeTxtFile(sentence, "./", fileNameCourt);
                                    } catch (IOException ex) {
                                        Logger.getLogger(MainVerticle.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                    log.info(eleArr.getString(j));
                                }
                            }
                            //log.info("Received response with status code" + String.valueOf(rsp.statusCode()) + rsp.bodyAsString());
                        } else {
                            log.info("Something went wrong " + ar.cause().getMessage());
                        }
                    });

        }

        routingContext.response().end();

        JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
                .put("filename", routingContext.request().getParam("filename"))
                .put("uri", routingContext.request().getParam("uri"));

        vertx.eventBus().request("file.upload", message, resp -> {
            if (resp.failed()) {
                response.setStatusCode(400);
                //response.end(resp.cause().toString());
            } else {
                JsonObject file = (JsonObject) resp.result().body();

                if (file.containsKey("error")) {
                    response.setStatusCode(400);
                    //response.end(file.encode());
                } else {
                    response.setStatusCode(200);
                    //response.end(file.encode());
                }
            }
        });

    }

    private void handleConsultFile(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        JsonObject message = new JsonObject().put("filename", routingContext.request().getParam("filename"));

        vertx.eventBus().request("file.consult", message, resp -> {
            if (resp.failed()) {
                response.setStatusCode(400);
                response.end(resp.cause().toString());
            } else {

                JsonObject file = (JsonObject) resp.result().body();

                if (file.containsKey("error")) {
                    response.setStatusCode(404);
                    response.end("file not found");
                } else {
                    response.end(file.encode());
                }
            }
        });
    }

    private void handleDeleteFile(RoutingContext routingContext) {
        HttpServerResponse response = routingContext.response();
        JsonObject message = new JsonObject().put("filename", routingContext.request().getParam("filename"));

        vertx.eventBus().request("file.delete", message, resp -> {
            if (resp.failed()) {
                response.setStatusCode(400);
                response.end(resp.cause().toString());
            } else {
                Boolean result = (Boolean) resp.result().body();
                if (result) {
                    response.setStatusCode(200);
                    response.end("file deleted successfully");
                } else {
                    response.setStatusCode(400);
                    response.end("cannot delete file");
                }
            }
        });
    }

}
