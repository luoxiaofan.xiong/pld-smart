package fr.insalyon.hex4234.cours;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    Router router = Router.router(vertx);
    HttpServer server = vertx.createHttpServer();

    router.route("/course/consult").handler(this::handleConsultCourse);

    router.route("/course/add").handler(this::handleAddCourse);
    // this will put the course information into RoutingContext
    router.route("/course/modify").handler(this::handleModifyCourse);
    // this will update the course information into RoutingContext
    router.route("/course/delete/:courseid").handler(this::handleDeleteCourse);
    // this will put the course information into RoutingContext

    server.requestHandler(router).listen(8889);

    vertx.deployVerticle(new CoursVerticle());

  }

  private void handleAddCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    // if
    // (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
        .put("coursename", routingContext.request().getParam("coursename"))
        .put("description", routingContext.request().getParam("description"))
        .put("professors", new JsonArray(routingContext.request().params().getAll("professors")))
        .put("year", routingContext.request().getParam("year"));
    vertx.eventBus().request("course.add", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        Boolean result = (Boolean) resp.result().body();
        if (result) {
          response.setStatusCode(200);
          response.end("added successfully");
        } else {
          response.setStatusCode(400);
          response.end("course exists");
        }
      }
    });
    /*
     * } else{ response.setStatusCode(401); }
     */
  }

  private void handleConsultCourse(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
        .put("year", routingContext.request().getParam("year"))
        .put("professors", new JsonArray(routingContext.request().params().getAll("professors")));

    vertx.eventBus().request("course.consult", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        JsonObject course = (JsonObject) resp.result().body();

        if (course.containsKey("error")) {
          response.setStatusCode(404);
          response.end("course not found");
        } else {
          response.end(course.encode());
        }
      }
    });
  }

  private void handleModifyCourse(RoutingContext routingContext) {
    // check if it's admin
    HttpServerResponse response = routingContext.response();
    // if
    // (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"))
        .put("coursename", routingContext.request().getParam("coursename"))
        .put("description", routingContext.request().getParam("description"))
        .put("professors", new JsonArray(routingContext.request().params().getAll("professors")))
        .put("year", routingContext.request().getParam("year"));
    vertx.eventBus().request("course.modify", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        Boolean result = (Boolean) resp.result().body();
        if (result) {
          response.setStatusCode(200);
          response.end("modified successfully");
        } else {
          response.setStatusCode(400);
          response.end("course does not exists");
        }
      }
    });
    /*
     * } else{ response.setStatusCode(401); }
     */
  }

  private void handleDeleteCourse(RoutingContext routingContext) {
    // check if it's admin
    HttpServerResponse response = routingContext.response();
    // if
    // (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
    JsonObject message = new JsonObject().put("courseid", routingContext.request().getParam("courseid"));
    vertx.eventBus().request("course.delete", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
      } else {
        Boolean result = (Boolean) resp.result().body();
        if (result) {
          response.setStatusCode(200);
          response.end("deleted successfully");
        }
      }
    });
    /*
     * } else{ response.setStatusCode(401); }
     */
  }

}
