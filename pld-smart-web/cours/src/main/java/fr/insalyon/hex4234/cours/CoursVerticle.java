package fr.insalyon.hex4234.cours;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

public class CoursVerticle extends AbstractVerticle {
  private MongoClient client = null;

  private static final String COLLECTION = "course";

  @Override
  public void start(Future<Void> startFuture) throws Exception {
    JsonObject mongoConfig = new JsonObject().put("db_name", "course").put("host", "127.0.0.1").put("port", 27017);
    client = MongoClient.create(vertx, mongoConfig);
    // create course and set index if not done
    findCourseTable().<Void>compose(exists -> {
      if (exists) {
        // Do nothing;
        Promise<Void> promise = Promise.promise();
        promise.complete();
        return promise.future();
      } else {
        // create a collection "course" and then add index
        return createCollection().<Void>compose(v -> createCourseIndex())
            .<Void>compose(v -> addCourse("sie", "SIE", "SIE", new JsonArray(Collections.unmodifiableList(new ArrayList<String>(Arrays.asList("admin")))), 2).<Void>map(null));
      }

    });
    EventBus eventbus = vertx.eventBus();

    // event bus for request around consult a course
    eventbus.<JsonObject>consumer("course.consult").handler(message -> {
      JsonObject object = message.body();
      getCourseInfo(object.getString("courseid"))
          .onSuccess(v -> message.reply(new JsonObject().put("result", v)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for request around consult a course
    eventbus.<JsonObject>consumer("admin.consultCourse").handler(message -> {
      JsonObject object = message.body();
      getCoursesInfo(object.getString("courseid"), object.getInteger("year"), object.getJsonArray("professors"))
          .onSuccess(v -> message.reply(new JsonObject().put("result", v)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for request around adding a course
    eventbus.<JsonObject>consumer("admin.addCourse").handler(message -> {
      JsonObject object = message.body();
      addCourse(object.getString("courseid"), object.getString("coursename"), object.getString("description"),
          object.getJsonArray("professors"),  object.getInteger("year"))
              .onSuccess(v -> message.reply(new JsonObject().put("success", v)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });
    // event bus for request around modifying a course
    eventbus.<JsonObject>consumer("admin.updateCourse").handler(message -> {
      JsonObject object = message.body();
      modifyCourse(object.getString("courseid"), object.getString("coursename"), object.getString("description"),
          object.getJsonArray("professors"), Integer.parseInt(object.getString("year")))
              .onSuccess(v -> message.reply(new JsonObject().put("success", v)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for request around deleting a course
    eventbus.<JsonObject>consumer("admin.deleteCourse").handler(message -> {
      JsonObject object = message.body();
      deleteCourse(object.getString("courseid")).onSuccess(v -> message.reply(new JsonObject().put("success", v)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });
  }

  private Future<Boolean> findCourseTable() {
    Promise<Boolean> promise = Promise.promise();
    client.getCollections(event -> {
      // if it fails, we pass along the cause
      if (event.failed()) {
        promise.fail(event.cause());
      }
      // otherwise, we pass the result
      else {
        promise.complete(event.result().contains(COLLECTION));
      }
    });
    // give back the future object representing this promise
    return promise.future();
  }

  /**
   * Create the course table if not exists
   */
  private Future<Void> createCollection() {
    Promise<Void> promise = Promise.promise();
    client.createCollection(COLLECTION, event -> {
      if (event.failed()) {
        promise.fail(event.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Create the course index
   */
  private Future<Void> createCourseIndex() {
    Promise<Void> promise = Promise.promise();
    client.createIndexWithOptions(COLLECTION, new JsonObject().put("courseid", 1), new IndexOptions().unique(true),
        event -> {
          if (event.failed()) {
            promise.fail(event.cause());
          } else {
            promise.complete();
          }
        });
    return promise.future();
  }

  /**
   * Add Course
   */
  private Future<Boolean> addCourse(String courseid, String coursename, String description, JsonArray professors,
      Integer year) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject course = new JsonObject().put("courseid", courseid);
    Optional.ofNullable(coursename).ifPresent(s -> course.put("coursename", s));
    Optional.ofNullable(description).ifPresent(s -> course.put("description", s));
    Optional.ofNullable(year).ifPresent(s -> course.put("year", s));
    Optional.ofNullable(professors).ifPresent(p -> course.put("professors", p));
    client.insert(COLLECTION, course, result -> {
      if (result.succeeded()) {
        promise.complete(true);
      } else {
        promise.fail(result.cause());
      }
    });
    return promise.future();
  }

  /**
   * Get Courses Info
   */
  private Future<JsonObject> getCourseInfo(String courseid) {
    Promise<JsonObject> promise = Promise.promise();
    JsonObject courseInfo = new JsonObject();
    courseInfo.put("courseid", courseid);

    JsonObject fields = new JsonObject().put("courseid", true).put("coursename", true).put("description", true).put("professors", true);

    client.findOne(COLLECTION, courseInfo, fields, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result());
      }
    });
    return promise.future();

  }

  /**
   * Get Courses Info
   */
  private Future<JsonArray> getCoursesInfo(String courseid, Integer year, JsonArray profid) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject courseInfo = new JsonObject();
    if (courseid != null)
      courseInfo.put("courseid", courseid);
    if (year != null)
      courseInfo.put("year", year);
    if (profid != null)
      courseInfo.put("professors", new JsonObject().put("$all", profid));

    client.find(COLLECTION, courseInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(new JsonArray(res.result()));
      }
    });
    return promise.future();

  }

  /**
   * Delete Course
   */
  private Future<Boolean> deleteCourse(String courseid) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject courseInfo = new JsonObject().put("courseid", courseid);
    client.removeDocument(COLLECTION, courseInfo, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result().getRemovedCount() == 1);
      }
    });
    return promise.future();

  }

  /**
   * Modify Course
   */
  private Future<Boolean> modifyCourse(String courseid, String coursename, String description, JsonArray professors,
      Integer year) {
    Promise<Boolean> promise = Promise.promise();
    JsonObject course = new JsonObject().put("courseid", courseid);
    JsonObject courseUpdate = new JsonObject();
    Optional.ofNullable(coursename).ifPresent(s -> courseUpdate.put("coursename", s));
    Optional.ofNullable(description).ifPresent(s -> courseUpdate.put("description", s));
    Optional.ofNullable(professors).ifPresent(s -> courseUpdate.put("professors", s));
    client.findOneAndUpdate(COLLECTION, course, new JsonObject().put("$set", courseUpdate), result -> {
      if (result.succeeded()) {
        promise.complete(!result.result().isEmpty());
      } else {
        promise.fail(result.cause());
      }
    });
    return promise.future();
  }

}
