package fr.insalyon.hex4234.utilisateur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

import io.vertx.ext.auth.User;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.mongo.MongoAuth;
import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

import fr.insalyon.hex4234.common.*;

public class CredentialVerticle extends AbstractVerticle {

  private MongoClient client = null;

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    JsonObject mongoConfig = new JsonObject().put("db_name", "user").put("host", "127.0.0.1").put("port", 27017);
    client = MongoClient.create(vertx, mongoConfig);

    // create user and set index if not done
    Future<Void> initialDB = findUserTable().<Void>compose((Boolean exists) -> {
      if (exists) {
        // Do nothing;
        Promise<Void> promise = Promise.promise();
        promise.complete();
        return promise.future();
      } else {
        return createCollection().<Void>compose(v -> createUserIndex())
            .<Void>compose(v -> addUser("admin", "admin", Collections.unmodifiableList(new ArrayList<String>(Arrays.asList("admin", "professor", "student")))).<Void>map(null));
      }
    });

    EventBus eventbus = vertx.eventBus();

    // event bus for request around login a user
    eventbus.<JsonObject>consumer("user.login").handler(message -> {
      JsonObject object = message.body();
      if (!object.containsKey("username") || !object.containsKey("password") || object.getString("username") == null
          || object.getString("password") == null) {
        message.fail(400, object.encode());
      } else {
        login(object.getString("username"), object.getString("password"))
            .onSuccess(token -> message.reply(new JsonObject().put("token", token)))
            .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
      }
    });

    // event bus for updating user's information
    eventbus.<JsonObject>consumer("user.updatePassword").handler(message -> {
      JsonObject object = message.body();
      if (!object.containsKey("username") || !object.containsKey("oldPassword") || !object.containsKey("newPassword")
          || object.getString("username") == null || object.getString("oldPassword") == null
          || object.getString("newPassword") == null) {
        message.fail(400, object.encode());
      } else {
        authenticate(object.getString("username"), object.getString("oldPassword"))
            .compose(user -> updatePassword(user, object.getString("username"), object.getString("newPassword")))
            .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
            .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
      }
    });

    eventbus.<JsonObject>consumer("admin.updateUserCred").handler(message -> {
      JsonObject object = message.body();
      Future<Void> updatepassword = null;
      if (object.containsKey("password") && object.getString("password") != null) {
        updatepassword = updatePassword(object.getString("username"), object.getString("password"));
      } else {
        updatepassword = Future.succeededFuture();
      }
      Future<Void> updaterole = null;
      if (object.containsKey("roles") && object.getJsonArray("roles") != null) {
        updaterole = updateRole(object.getString("username"), object.getJsonArray("roles"));
      } else {
        updaterole = Future.succeededFuture();
      }
      CompositeFuture.all(updatepassword, updaterole)
          .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for admin to add user
    eventbus.<JsonObject>consumer("admin.addUserCred").handler(message -> {
      JsonObject object = message.body();
      if (!object.containsKey("username") || !object.containsKey("roles") || !object.containsKey("password")
          || object.getString("username") == null || object.getJsonArray("roles") == null
          || object.getString("password") == null) {
        message.fail(400, object.encode());
      } else {
        addUser(object.getString("username"), object.getString("password"),
            (List<String>) object.getJsonArray("roles").getList())
                .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
      }
    });

    initialDB.onSuccess(v -> startPromise.complete());
  }

  /**
   * Find if the user table exists
   */
  private Future<Boolean> findUserTable() {
    Promise<Boolean> promise = Promise.promise();
    client.getCollections(event -> {
      // if it fails, we pass along the cause
      if (event.failed()) {
        promise.fail(event.cause());
      }
      // otherwise, we pass the result
      else {
        promise.complete(event.result().contains(MongoAuth.DEFAULT_COLLECTION_NAME));

      }
    });
    // give back the future object representing this promise
    return promise.future();
  }

  /**
   * Create the user table if not exists
   */
  private Future<Void> createCollection() {
    Promise<Void> promise = Promise.promise();
    client.createCollection(MongoAuth.DEFAULT_COLLECTION_NAME, event -> {
      if (event.failed()) {
        promise.fail(event.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Create the user index for password
   */
  private Future<Void> createUserIndex() {
    Promise<Void> promise = Promise.promise();
    client.createIndexWithOptions(MongoAuth.DEFAULT_COLLECTION_NAME,
        new JsonObject().put(MongoAuth.DEFAULT_USERNAME_FIELD, 1), new IndexOptions().unique(true), event -> {
          if (event.failed()) {
            promise.fail(event.cause());
          } else {
            promise.complete();
          }
        });
    return promise.future();
  }

  private Future<User> authenticate(String username, String password) {
    Promise<User> promise = Promise.promise();
    MongoAuth auth = MongoAuth.create(client, new JsonObject());
    JsonObject authInfo = new JsonObject().put(MongoAuth.DEFAULT_USERNAME_FIELD, username)
        .put(MongoAuth.DEFAULT_PASSWORD_FIELD, password);
    auth.authenticate(authInfo, result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        promise.complete(result.result());
      }
    });
    return promise.future();
  }

  /**
   * Login user
   */
  private Future<String> login(String username, String password) {
    return authenticate(username, password).map(user -> {
      JWTAuth auth = Util.getJWTAuth(vertx);
      JsonObject userObject = user.principal();
      userObject.remove("salt");
      userObject.remove("password");
      return auth.generateToken(userObject);
    });
  }

  /**
   * Update Information
   */
  private Future<Void> updatePassword(User user, String username, String newPassword) {
    Promise<Void> promise = Promise.promise();
    JsonObject query = new JsonObject().put(MongoAuth.DEFAULT_USERNAME_FIELD, username);
    JsonObject replace = new JsonObject().put(MongoAuth.DEFAULT_PASSWORD_FIELD,
        MongoAuth.create(client, new JsonObject()).getHashStrategy().computeHash(newPassword, user));
    client.findOneAndUpdate(MongoAuth.DEFAULT_COLLECTION_NAME, query, new JsonObject().put("$set", replace), result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Update password for admin
   * @param username
   * @param newPassword
   * @return
   */
  private Future<Void> updatePassword(String username, String newPassword) {
    Promise<Void> promise = Promise.promise();
    JsonObject query = new JsonObject().put(MongoAuth.DEFAULT_USERNAME_FIELD, username);
    client.findOneAndDelete(MongoAuth.DEFAULT_COLLECTION_NAME, query, result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        JsonObject object = result.result();
        MongoAuth auth = MongoAuth.create(client, new JsonObject());
        auth.insertUser(username, newPassword, object.getJsonArray("roles").getList(), Collections.emptyList(), res -> {
          if (res.failed()) {
            promise.fail(res.cause());
          }
          else {
            promise.complete();
          }
        });
      }
    });
    return promise.future();
  }


  private Future<Void> updateRole(String username, JsonArray roles) {
    Promise<Void> promise = Promise.promise();

    JsonObject query = new JsonObject().put(MongoAuth.DEFAULT_USERNAME_FIELD, username);
    JsonObject replace = new JsonObject().put(MongoAuth.DEFAULT_ROLE_FIELD, roles);
    client.findOneAndUpdate(MongoAuth.DEFAULT_COLLECTION_NAME, query, new JsonObject().put("$set", replace), result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Admin : add user
   */
  private Future<Void> addUser(String username, String password, List<String> role) {
    Promise<Void> promise = Promise.promise();
    MongoAuth auth = MongoAuth.create(client, new JsonObject());
    auth.insertUser(username, password, role, Collections.emptyList(), result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }
}