package fr.insalyon.hex4234.utilisateur;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.EventBus;

import io.vertx.core.json.JsonObject;
import io.vertx.core.json.JsonArray;

import io.vertx.ext.mongo.IndexOptions;
import io.vertx.ext.mongo.MongoClient;

public class UserInfoVerticle extends AbstractVerticle {

  private MongoClient client = null;
  private static final String COLLECTION = "userinfo";

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    JsonObject mongoConfig = new JsonObject().put("db_name", "user").put("host", "127.0.0.1").put("port", 27017);
    client = MongoClient.create(vertx, mongoConfig);

    // create user and set index if not done
    findUserTable().<Void>compose((Boolean exists) -> {
      if (exists) {
        // Do nothing;
        Promise<Void> promise = Promise.promise();
        promise.complete();
        return promise.future();
      } else {
        return createCollection().<Void>compose(v -> createUserIndex()).<Void>compose(
            v -> addUser("admin", new JsonArray(Collections.unmodifiableList(new ArrayList<String>(Arrays.asList("admin", "professor", "student")))), Optional.empty())
                .<Void>map(null));
      }

    });

    EventBus eventbus = vertx.eventBus();

    eventbus.<JsonObject>consumer("user.getInformation").handler(message -> {
      JsonObject object = message.body();
      getUserInfo(object.getString("username")).onSuccess(user -> message.reply(user))
          .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for updating user's information
    eventbus.<JsonObject>consumer("user.updateInformation").handler(message -> {
      JsonObject object = message.body();
      if (object.containsKey("oldPassword") && object.containsKey("newPassword")) {
        eventbus.request("user.updatePassword", object, result -> {
          if (result.failed()) {
            result.cause().printStackTrace();
            message.reply(new JsonObject().put("error", result.cause().getMessage()));
          } else {
            updateUserInformation(object.getString("username"), Optional.ofNullable(object.getString("firstname")),
                Optional.ofNullable(object.getString("lastname")), Optional.ofNullable(object.getString("email")),
                Optional.empty(), Optional.empty()).onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                    .onFailure((Throwable error) -> message.reply(new JsonObject().put("error", error.getMessage())));
          }
        });
      } else {
        updateUserInformation(object.getString("username"), Optional.ofNullable(object.getString("firstname")),
            Optional.ofNullable(object.getString("lastname")), Optional.ofNullable(object.getString("email")),
            Optional.empty(), Optional.empty()).onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                .onFailure((Throwable error) -> message.reply(new JsonObject().put("error", error.getMessage())));
      }

    });

    eventbus.<JsonObject>consumer("admin.consultUser").handler(message -> {
      JsonObject object = message.body();
      consultUserInformation(Optional.ofNullable(object.getString("username")),
          Optional.ofNullable(object.getJsonArray("role")), Optional.ofNullable(object.getInteger("year")))
              .onSuccess(array -> message.reply(new JsonObject().put("result", array)))
              .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
    });

    // event bus for admin to add user
    eventbus.<JsonObject>consumer("admin.addUser").handler(message -> {
      JsonObject object = message.body();
      eventbus.request("admin.addUserCred", object, result -> {
        if (result.failed()) {
          result.cause().printStackTrace();
          message.reply(new JsonObject().put("error", result.cause().getMessage()));
        } else {
          addUser(object.getString("username"), object.getJsonArray("roles"),
              Optional.ofNullable(object.getInteger("year")))
                  .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                  .onFailure(error -> message.reply(new JsonObject().put("error", error.getMessage())));
        }
      });
    });

    // event bus for admin to update user
    eventbus.<JsonObject>consumer("admin.updateUser").handler(message -> {
      JsonObject object = message.body();
      eventbus.request("admin.updateUserCred", object, result -> {
        updateUserInformation(object.getString("username"), Optional.empty(), Optional.empty(), Optional.empty(),
            Optional.ofNullable(object.getJsonArray("roles")), Optional.ofNullable(object.getInteger("year")))
                .onSuccess(v -> message.reply(new JsonObject().put("success", true)))
                .onFailure((Throwable error) -> message.reply(new JsonObject().put("error", error.getMessage())));
      });

    });

  }

  /**
   * Find if the user table exists
   */
  private Future<Boolean> findUserTable() {
    Promise<Boolean> promise = Promise.promise();
    client.getCollections(event -> {
      // if it fails, we pass along the cause
      if (event.failed()) {
        promise.fail(event.cause());
      }
      // otherwise, we pass the result
      else {
        promise.complete(event.result().contains(COLLECTION));

      }
    });
    // give back the future object representing this promise
    return promise.future();
  }

  /**
   * Create the user table if not exists
   */
  private Future<Void> createCollection() {
    Promise<Void> promise = Promise.promise();
    client.createCollection(COLLECTION, event -> {
      if (event.failed()) {
        promise.fail(event.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

  /**
   * Create the user index for password
   */

  private Future<Void> createUserIndex() {
    Promise<Void> promise = Promise.promise();
    client.createIndexWithOptions(COLLECTION, new JsonObject().put("username", 1), new IndexOptions().unique(true),
        event -> {
          if (event.failed()) {
            promise.fail(event.cause());
          } else {
            promise.complete();
          }
        });
    return promise.future();
  }

  /**
   * get user info
   */
  private Future<JsonObject> getUserInfo(String username) {
    Promise<JsonObject> promise = Promise.promise();
    JsonObject query = new JsonObject().put("username", username);
    JsonObject projection = new JsonObject().put("username", true).put("firstname", true).put("lastname", true)
        .put("email", true).put("year", true).put("roles", true);
    client.findOne(COLLECTION, query, projection, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete(res.result());
      }
    });
    return promise.future();
  }

  /**
   * update information
   */
  private Future<Void> updateUserInformation(String username, Optional<String> firstname, Optional<String> lastname,
      Optional<String> email, Optional<JsonArray> roles, Optional<Integer> year) {
    Promise<Void> promise = Promise.promise();
    JsonObject query = new JsonObject().put("username", username);
    JsonObject update = new JsonObject();
    firstname.ifPresent(s -> update.put("firstname", s));
    lastname.ifPresent(s -> update.put("lastname", s));
    email.ifPresent(s -> update.put("email", s));
    roles.ifPresent(r -> update.put("roles", r));
    year.ifPresent(i -> update.put("year", i));
    if (update.isEmpty()) {
      promise.complete();
    } else {
      client.findOneAndUpdate(COLLECTION, query, new JsonObject().put("$set", update), res -> {
        if (res.failed()) {
          promise.fail(res.cause());
        } else {
          promise.complete();
        }
      });
    }

    return promise.future();
  }

  /**
   * Admin : consult User Information
   */

  private Future<JsonArray> consultUserInformation(Optional<String> username, Optional<JsonArray> role,
      Optional<Integer> year) {
    Promise<JsonArray> promise = Promise.promise();
    JsonObject query = new JsonObject();
    username.ifPresent(s -> query.put("username", s));
    role.ifPresent(r -> query.put("roles", r));
    year.ifPresent(i -> query.put("year", i));

    client.find(COLLECTION, query, result -> {
      if (result.failed()) {
        promise.fail(result.cause());
      } else {
        promise.complete(new JsonArray(result.result()));
      }
    });

    return promise.future();
  }

  /**
   * Admin : add user
   */
  private Future<Void> addUser(String username, JsonArray role, Optional<Integer> year) {
    Promise<Void> promise = Promise.promise();
    JsonObject newUser = new JsonObject().put("username", username).put("roles", role);
    year.ifPresent(i -> newUser.put("year", i));
    client.insert(COLLECTION, newUser, res -> {
      if (res.failed()) {
        promise.fail(res.cause());
      } else {
        promise.complete();
      }
    });
    return promise.future();
  }

}