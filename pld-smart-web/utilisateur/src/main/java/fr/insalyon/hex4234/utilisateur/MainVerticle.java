package fr.insalyon.hex4234.utilisateur;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.JWTAuthHandler;
import fr.insalyon.hex4234.common.Util;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    Router router = Router.router(vertx);
    HttpServer server = vertx.createHttpServer();

    // For tests without GUI
    // request : login 
    router.get("/login").handler(this::handleLogin);
    // request : get user info
    router.get("/info").handler(this::handleUserInformation);
    // request : update user info
    router.get("/modifInfo").handler(this::handleUserUpdate);
    // request : admin - consult user
    router.get("/user").handler(this::handleUserInformationAdmin);
    // request : admin - add user
    router.get("/addUser").handler(this::handleAddUserAdmin);
    // request : admin - update user
    router.get("/updateUser").handler(this::handleUpdateUserAdmin);

    // Real request 
    /*
    // request : login 
    router.get("/login").handler(this::handleLogin);
    // request : get user info
    router.get("/info").handler(this::handleUserInformation);
    // request : update user info
    router.put("/modifInfo").handler(this::handleUserUpdate);
    // request : admin - consult user
    router.get("/user").handler(this::handleUserInformationAdmin);
    // request : admin - add user
    router.put("/addUser").handler(this::handleAddUserAdmin);
    // request : admin - update user
    router.post("/updateUser").handler(this::handleUpdateUserAdmin);
    */

    server.requestHandler(router).listen(8080);

    vertx.deployVerticle(new CredentialVerticle());
  }

  /**
   * login
   */
  private void handleLogin(RoutingContext routingContext) {
    HttpServerResponse response = routingContext.response();
    JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username")).put("password",
        routingContext.request().getParam("password"));
    vertx.eventBus().request("user.login", message, resp -> {
      if (resp.failed()) {
        response.setStatusCode(400);
        response.end(resp.cause().toString());
        
      } else {
        Boolean result = (Boolean) resp.result().body();
        
        if (result) {
          response.setStatusCode(200);
          response.end("user is logged.");
        } else {
          response.setStatusCode(400);
          response.end("A problem occured.");
        }
      }
    });
  }

  /**
   * Get Information of User himself
   */

  private void handleUserInformation(RoutingContext routingContext)
  {
    HttpServerResponse response = routingContext.response();
    // S'il y a un username
    //if ( ! routingContext.user().principal().getString("username").isEmpty()) {
      // for debugging 
      String usernameToSearch = routingContext.request().getParam("username");
      
      /* real version 
      String usernameToSearch = routingContext.user().principal().getString("username");
      */
      JsonObject message = new JsonObject().put("username", usernameToSearch);

      vertx.eventBus().request("user.getInformation", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          JsonObject user = (JsonObject) resp.result().body();
          if (user.containsKey("error")) {
            response.setStatusCode(401);
            response.end(user.getString("error"));
          } else {
            response.setStatusCode(200);
            response.end(user.getString("result"));
          }
        }
      });

    /*
    } else {
      response.setStatusCode(401);
      response.end("Not user's accound");
    }
    */
  }

  /**
   * 
   * update user information
   */
  private void handleUserUpdate(RoutingContext routingContext){
    HttpServerResponse response = routingContext.response();

    String usernameToSearch = routingContext.request().getParam("username");

    if (usernameToSearch == null)
    {
      response.setStatusCode(401);
      response.end("Unauthorized.");
      return;
    }

    // To avoid null pointer exception
    String newpassword = "";
    if (routingContext.request().getParam("newpassword") != null)
      newpassword = routingContext.request().getParam("newpassword");

    String oldpassword = "";
    if (routingContext.request().getParam("oldpassword") != null)
      oldpassword = routingContext.request().getParam("oldpassword");

    String firstname = "";
    if (routingContext.request().getParam("firstname") != null)
      firstname = routingContext.request().getParam("firstname");

    String lastname = "";
    if (routingContext.request().getParam("lastname") != null)
      lastname = routingContext.request().getParam("lastname");

    String email = "";
    if (routingContext.request().getParam("email") != null)
      email = routingContext.request().getParam("email");


    // on vérifie que la personne qui invoque la requête est celle qui est concernée
    //if (usernameToSearch == routingContext.user().principal().getString("username")){
      JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username"))
                                            .put("oldpassword", oldpassword)
                                            .put("newpassword", newpassword)
                                            .put("firstname", firstname)
                                            .put("lastname", lastname)
                                            .put("email", email);
      
      vertx.eventBus().request("user.UpdateInformation", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          Boolean result = (Boolean) resp.result().body();
          if (result) {
            response.setStatusCode(200);
            response.end("user was modified.");
          } else {
            response.setStatusCode(400);
            response.end("A problem occured.");
          }
        }
      });  
    //}
  }

  /**
   *  Admin : Get User Information
   */
  private void handleUserInformationAdmin(RoutingContext routingContext)
  {
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
      // if it is an admin
      // To avoid null pointer exception
      String year = "-1";
      if (routingContext.request().getParam("year") != null)
        year = routingContext.request().getParam("year");

      String username = "";
      if (routingContext.request().getParam("username") != null)
        username = routingContext.request().getParam("username");

      JsonObject message = new JsonObject().put("username", username)
                                          .put("role", routingContext.request().params().getAll("role"))
                                          .put("year", year);
      vertx.eventBus().request("admin.consultUser", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          JsonObject userToFind = (JsonObject) resp.result().body();
          if (userToFind.containsKey("error")) {
            response.setStatusCode(401);
            response.end(userToFind.getString("error"));
          } else {
            response.setStatusCode(200);
            response.end(userToFind.getString("result"));
          }
        }
      });
    /*
    } else {
      response.setStatusCode(401);
      response.end("Not admin. Not authorized");
    }
    */
  }

  /**
   * Admin : add user
   */
  
  private void handleAddUserAdmin (RoutingContext routingContext)
  {
    HttpServerResponse response = routingContext.response();
    //if (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
      // if it is an admin
      if (routingContext.request().getParam("username") == null || routingContext.request().getParam("password") == null)
      {
        response.setStatusCode(400);
        response.end("Bad request. Need username or password.");
        return;
      }

      // To avoid null pointer exception
      String year = "-1";
      if (routingContext.request().getParam("year") != null)
        year = routingContext.request().getParam("year");
      
      JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username"))
                                          .put ("password", routingContext.request().getParam("password"))
                                          .put("role", routingContext.request().params().getAll("role"))
                                          .put("year", year);
      vertx.eventBus().request("admin.addUser", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          Boolean result = (Boolean) resp.result().body();
          if (result) {
            response.setStatusCode(200);
            response.end("user was added.");
          } else {
            response.setStatusCode(400);
            response.end("A problem occured.");
          }
        }
      });
    /*
    } else {
      response.setStatusCode(401);
      response.end("Not admin. Not authorized");
    }
    */
  }

  /**
   * Admin : update user
   */

  private void handleUpdateUserAdmin (RoutingContext routingContext)
  {
    HttpServerResponse response = routingContext.response();
    // if it is an admin
    if (routingContext.request().getParam("username") == null || routingContext.request().getParam("password") == null)
    {
      response.setStatusCode(400);
      response.end("Bad request. Need username.");
      return;
    }

    //if (routingContext.user().principal().getJsonArray("roles").contains("admin")) {
      // if it is an admin

      // To avoid null pointer exception
      String year = "-1";
      if (routingContext.request().getParam("year") != null)
        year = routingContext.request().getParam("year");

      String password = "";
      if (routingContext.request().getParam("password") != null)
        password = routingContext.request().getParam("password");

      JsonObject message = new JsonObject().put("username", routingContext.request().getParam("username"))
                                          .put ("password", password)
                                          .put("role", routingContext.request().params().getAll("role"))
                                          .put("year", year);
      vertx.eventBus().request("admin.updateUser", message, resp -> {
        if (resp.failed()) {
          response.setStatusCode(400);
          response.end(resp.cause().toString());
        } else {
          Boolean result = (Boolean) resp.result().body();
          if (result) {
            response.setStatusCode(200);
            response.end("user object was updated.");
          } else {
            response.setStatusCode(400);
            response.end("A problem occured.");
          }
        }

      });
    /*
    } else {
      response.setStatusCode(401);
      response.end("Not admin. Not authorized.");
    }
    */

  }



}