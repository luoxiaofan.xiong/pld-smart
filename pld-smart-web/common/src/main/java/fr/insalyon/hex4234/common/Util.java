package fr.insalyon.hex4234.common;

import io.vertx.core.Vertx;
import io.vertx.ext.auth.KeyStoreOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;

public class Util {
    public static JWTAuth getJWTAuth(Vertx vertx) {
        JWTAuth provider = JWTAuth.create(vertx, new JWTAuthOptions()
            .setKeyStore(new KeyStoreOptions()
                .setPath("keystore.jceks")
                .setType("jceks")
                .setPassword("secret")
            ));
        return provider;
    }
}